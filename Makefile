all:
	gcc -fPIC -std=c99 -O2 -shared -o libear.so ear.c -ldl -lpthread
clean:
	rm -f libear.so
install: all
	cp -f libear.so bear ~/workspace/git/sbin/
